package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ShoppingCartTest {
    ShoppingCart shoppingCart = new ShoppingCart();
    @Test
    void test() {
        Product product1 = new Product("Milk",2,100);
        Product product2 = new Product("Butter",2,100);
        Product product3 = new Product("Eggs",2,100);
        Product product4 = new Product("Juice",3,100);
        Product product5 = new Product("Cola",3,100);
        Product product6 = new Product("Rum",100,1);
        Product product7 = new Product("",0,-1);

        assertTrue(shoppingCart.addProducts(product1.getName(), product1.getPrice(), product1.getQuantity()));
        assertTrue(shoppingCart.addProducts(product2.getName(), product2.getPrice(), product2.getQuantity()));
        assertTrue(shoppingCart.addProducts(product3.getName(), product3.getPrice(), product3.getQuantity()));
        assertTrue(shoppingCart.addProducts(product4.getName(), product4.getPrice(), product4.getQuantity()));
        assertTrue(shoppingCart.addProducts(product5.getName(), product5.getPrice(), product5.getQuantity()));
        assertFalse(shoppingCart.addProducts(product6.getName(), product6.getPrice(), product6.getQuantity()));
        assertFalse(shoppingCart.addProducts(product7.getName(), product7.getPrice(), product7.getQuantity()));

    }
    @Test
    void deleteProductTest(){

        Product product1 = new Product("Milk",2,3);
        Product product2 = new Product("Water",1,3);
        Product product3 = new Product("Vodka",32,1);
        shoppingCart.addProducts(product1.getName(),product1.getPrice(),product1.getQuantity());
        shoppingCart.addProducts(product2.getName(),product2.getPrice(),product2.getQuantity());
        shoppingCart.addProducts(product3.getName(),product3.getPrice(),product3.getQuantity());

        assertTrue(shoppingCart.deleteProducts(product1.getName(), 2));
        assertTrue(shoppingCart.deleteProducts(product1.getName(), 1));
        assertFalse(shoppingCart.deleteProducts(product1.getName(), 1));
        assertFalse(shoppingCart.deleteProducts(product2.getName(), 4));
        assertFalse(shoppingCart.deleteProducts(product3.getName(), 4));
        assertFalse(shoppingCart.deleteProducts("Whiskey", 4));
    }
    @Test
    void getAllProductsNames(){
        shoppingCart.addProducts("Onion",4,3);
        shoppingCart.addProducts("Carrot",1,5);
        shoppingCart.addProducts("Potatos",2,10);
        shoppingCart.addProducts("Hand Cream",10,1);
        shoppingCart.addProducts("Potatos",10,2);

        List<String> items = new ArrayList<>();
        items.add("Potatos");
        items.add("Carrot");
        items.add("Onion");
        items.add("Hand Cream");

        assertEquals(items, shoppingCart.getProductsNames());
    }
    @Test
    void getQuantityByName() {
        Product product1 = new Product("Milk",2,3);
        Product product2 = new Product("Milk",2,3);
        Product product3 = new Product("Water",1,5);
        Product product4 = new Product("Vodka",32,1);
        shoppingCart.addProducts(product1.getName(),product1.getPrice(),product1.getQuantity());

        assertEquals(3, shoppingCart.getQuantityOfProduct(product1.getName()));

        shoppingCart.addProducts(product2.getName(),product2.getPrice(),product2.getQuantity());
        shoppingCart.addProducts(product3.getName(),product3.getPrice(),product3.getQuantity());
        shoppingCart.addProducts(product4.getName(),product4.getPrice(),product4.getQuantity());

        assertEquals(6, shoppingCart.getQuantityOfProduct(product2.getName()));
        assertEquals(5, shoppingCart.getQuantityOfProduct(product3.getName()));
        assertEquals(1, shoppingCart.getQuantityOfProduct(product4.getName()));

        shoppingCart.deleteProducts(product4.getName(),1);
        assertEquals(0, shoppingCart.getQuantityOfProduct(product4.getName()));
    }

    @Test
    void getProductPrice(){
        shoppingCart.addProducts("Milk",4,2);
        shoppingCart.addProducts("Water",2,12);
        shoppingCart.addProducts("Eggs",8,1);

        assertEquals(4,shoppingCart.getProductPrice("Milk"));
        assertEquals(2,shoppingCart.getProductPrice("Water"));
        assertEquals(8,shoppingCart.getProductPrice("Eggs"));
        assertEquals(0,shoppingCart.getProductPrice("Vodka"));
    }
    @Test
    void sumPriceOfProducts() {
        shoppingCart.addProducts("Bread",3,3);
        shoppingCart.addProducts("Cigarettes",18,1);
        shoppingCart.addProducts("Beer",3,6);
        shoppingCart.addProducts("Cola",5,2);
        shoppingCart.addProducts("Butter",5,1);

        assertEquals(60,shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("Cola",1);
        assertEquals(55,shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("Bread",4);
        assertEquals(55,shoppingCart.getSumProductsPrices());

    }
}

